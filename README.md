Official Blue Beauty Made for Steam

Official Blue Beauty is a Native Functioning skin for Valve's Steam client that seeks to improve the visuals and aesthetics of Steam beyond the default skin. 
It is extremely important you follow the instructions below:

1. Click the "Download" button next to the "Clone" button on this GitLab page select zip. That will download the skin's files.

2. Extract the files in the ZIP folder to:
    
Linux: the ~/.local/share/Steam/skins/ folder
   
 Mac: right-click Steam.app and choose show package contents then go to - /Contents/MacOS/skins
   
 Windows: the Program Files (x86)/Steam/skins folder

3. Open up Steam then open up the Steam Settings Window. Click on the Interface tab, and under the option that says "Select the skin you wish Steam to use (requires Steam to restart)", 
click on the dropdown, and select "official-blue-beauty-steam-skin-master". 
    

